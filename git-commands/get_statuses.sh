#!/bin/bash

debug=false
gits=*

function print {
	if [[ $debug = true ]];
	then
		echo "$1"
	fi
}

for git in $gits;
do
	if [[ -d $git ]];
	then
		print "GIT: "$git
		cd $git
		projs=*
		for proj in $projs;
		do
			print "	PROJ: "$proj
			cd $proj
			ei=false
			repos=*
			for repo in $repos;
			do
				print "		REPO: "$repo
				if [[ -d $repo && $repo != "extra-info" ]];
				then
					ei=true
					if [[ $debug = false ]];
					then
						echo -e "\e[100m"$repo"\e[0m"
						cd $repo
						git status
						echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
						cd "../"
					fi
				fi
			done
			if [[ ei = false ]];
			then
				echo -e "\e[31m"$proj" has'nt an extra-info directory\e[0m"
			fi
			cd "../"
		done
		cd "../"
	fi
done