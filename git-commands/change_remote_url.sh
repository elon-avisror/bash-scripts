#!/bin/bash

# Checking for this remote version
git remote -v

# For full remote origin details
git remote show origin

# Setting the new url
git remote set-url origin {INPUT}

# Adding a url (usually for the first time)
git remote add origin {INPUT}

# INPUT (by protocol)
https://{GIT}/{USERNAME}/{REPOSITORY].git #HTTPS
git@{GIT}:{USERNAME}/{REPOSITORY}.git #SSH
