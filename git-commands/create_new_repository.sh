# When you create a new repository locally, instead of going to GitLab to manually
# create a new project and then clone the repo
# locally, you can directly push it to GitLab to create the new project, all without leaving
# your terminal. If you have access rights to the associated namespace, GitLab will
# automatically create a new project under that GitLab namespace with its visibility
# set to Private by default (you can later change it in the project's settings).
# This can be done by using either SSH or HTTPS:

## Git push using SSH
git push --set-upstream git@gitlab.example.com:namespace/nonexistent-project.git master
#git push --set-upstream git@gitlab.com:elon-avisror/new-repo-name.git master

## Git push using HTTPS
#git push --set-upstream https://gitlab.example.com/namespace/nonexistent-project.git master

