#!/bin/bash

git init

git add -A

git commit "Make the backbone of the project"

git add remote $1
# The URL of the repository (from git)
# it's better to send the SSH URL (not HTTPS)

git push origin master
