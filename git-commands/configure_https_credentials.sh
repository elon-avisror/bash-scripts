#!/bin/bash

# You can add --global flag if you want to do it with all other git repositories in your machine
git config credential.helper store

# Goes to the local .git/config file (with the default editor, that defines on your machine)
git config -e
