#!/bin/bash

# Displays a list of the last "N" commits on the current branch
git rebase -i HEAD~{N}

# Replace "pick" with "reword" before each commit message you want to change.
# Example:
# "git rebase =i HEAD~3"
# Output (edit commits):
# "pick" e499d89 Delete CNAME
# "reword 0c39034 Better README
# "reword f7fde4a Change the commit message but push the same commit.

# Then execute this command (to say git it is not a simple push)
git push --force
