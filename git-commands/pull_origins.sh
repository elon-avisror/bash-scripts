#!/bin/bash

command="git pull"

function pull {
	cd $1
	${command}
}

pull "../access"
pull "../agpulse"
pull "../common"
pull "../starters"

# go back
cd "../extra-info"
