#!/bin/bash

# Getting from: https://dev.to/neshaz/how-to-git-stash-your-work-the-correct-way-cna#:~:text=Remove%20your%20stash&text=If%20you%20no%20longer%20need,with%3A%20%24%20git%20stash%20clear%20.

git stash -u

git stash list

git stash show
git stash show -p

git stash drop <stash_id>

git stash clear
