#!/bin/bash

name="Elon Avisror"
email="avisror.elon@gmail.com"
url="http://elon-avisror.com"

function npm_config_author {
	npm config set init.author.$1 "$2"
}

npm_config_author "name" "$name"
npm_config_author "email" "$email"
npm_config_author "url" "$url"
