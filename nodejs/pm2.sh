#!/bin/bash

pm2 start index.js --log app.log --time --cron "* * * * *" && pm2 save
