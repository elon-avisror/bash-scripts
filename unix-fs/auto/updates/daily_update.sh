#!/bin/bash

# $1 is the command itself (directed to apt)
function apt_command {
	echo -e "\e[40mapt "$1"\e[0m"
	sudo apt $1
}

# mandatory, update & upgrade "apt" (Advanced Packaging Tool)
#sudo apt update && sudo apt list --upgradeable && sudo apt upgrade
apt_command "update"
apt_command "list --upgradeable -a"
apt_command "upgrade"


# optional, comming from "https://itsfoss.com/apt-vs-apt-get-difference/"
#sudo apt autoremove && sudo apt full-upgrade
read -p "Do you want to remove an unused core libraries? [y/N] " unusedAnswer
unusedAnswer=${unusedAnswer:-N}

if [[ $unusedAnswer == "Y" || $unusedAnswer == "y" ]];
then
	apt_command "autoremove"
fi

# optional, check if there are some broken installations
read -p "Do you want to check if there are some broken installations? [y/N] " brokenAnswer
brokenAnswer=${brokenAnswer:-N}

if [[ $brokenAnswer == "Y" || $brokenAnswer == "y" ]];
then
	apt_command "--fix-broken install"
fi

# optional, start stable version of google-chrome
read -p "Do you want to start google-chrome-stable? [Y/n] " chromeAnswer
chromeAnswer=${chromeAnswer:-Y}
covid_19=true
if [[ $chromeAnswer == "Y" || $chromeAnswer == "y" ]];
then
	if [[ $covid_19 == true ]];
	then
		google-chrome-stable https://report.matrix-covid19.co.il/ >> /dev/null 2>&1 &
	else
		google-chrome-stable >> /dev/null 2>&1 &
	fi
fi
