#!/bin/bash

bak="../bak/"

# $1 - is what to copy
function backup {
	cp -r $1 ${bak}
}

backup ~/.bashrc
backup ~/.ssh
backup ~/.aws
backup ~/.gitconfig
backup ~/.profile
backup ~/.npmrc
backup ~/.selected_editor
