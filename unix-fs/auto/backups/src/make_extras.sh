#!/bin/bash

# globals
projects="/home/aelon/Documents/projects"		# from who to backup
ex="extra-info"						# what to backup
zip_file="/home/aelon/Documents/backups/bak/extras.zip"	# where to store the backup

# 1. cambium
cambium=${projects}"/cambium"

		# 1.a cambium->bitbucket
# header
cambium_bitbucket=${cambium}"/bitbucket"
# body
gmunch=${cambium_bitbucket}"/gmunch/"${ex}
weather=${cambium_bitbucket}"/weather/"${ex}
workspace=${cambium_bitbucket}"/workspace/"${ex}
# paunch-line
cambium_bitbucket_bk="${gmunch} ${weather} ${workspace}"

		# 1.b cambium->github
# header
cambium_github=${cambium}"/github"
# body
seev=${cambium_github}"/seev/"${ex}
youa=${cambium_github}"/youa/"${ex}
# paunch-line
cambium_github_bk="${seev} ${youa}"

		# 1.c cambium->gitlab
# header
cambium_gitlab=${cambium}"/gitlab"
# body
agpulse=${cambium_gitlab}"/agpulse/"${ex}
agrefactor=${cambium_gitlab}"/agrefactor/"${ex}
docker=${cambium_gitlab}"/docker/"${ex}
icl_weather_api=${cambium_gitlab}"/icl-weather-api/"${ex}
icl_weather_api_worker=${cambium_gitlab}"/icl-weather-api-worker/"${ex}
kg911=${cambium_gitlab}"/kg911/"${ex}
kneset_gil_crawler=${cambium_gitlab}"/kneset-gil-crawler/"${ex}
mot=${cambium_gitlab}"/mot/"${ex}
mylambda=${cambium_gitlab}"/mylambda/"${ex}
techtop=${cambium_gitlab}"/techtop/"${ex}
toto=${cambium_gitlab}"/toto/"${ex}
unidress=${cambium_gitlab}"/unidress/"${ex}
# paunch-line
cambium_gitlab_bk="${agpulse} ${agrefactor} ${docker} ${icl_weather_api} ${icl_weather_api_worker} ${kg911} ${kneset_gil_crawler} ${mot} ${mylambda} ${techtop} ${toto} ${unidress}"

# 2. ea
ea=${projects}"/ea"

		# 2.a ea->bitbucket
# header
ea_bitbucket=${ea}"/bitbucket"
# body
app_core=${ea_bitbucket}"/app-core/"${ex}
awesome_project=${ea_bitbucket}"/awesome-project/"${ex}
calculator_application=${ea_bitbucket}"/calculator-application/"${ex}
cyber_test=${ea_bitbucket}"/cyber-test/"${ex}
docker_images=${ea_bitbucket}"/docker-images/"${ex}
express_starter=${ea_bitbucket}"/express-starter/"${ex}
# paunch-line
ea_bitbucket_bk="${app_core} ${awesome_project} ${calculator_application} ${cyber_test} ${docker_images} ${express_starter} ${server_wrapper}"


		# 2.b ea->github
# header
ea_github=${ea}"/github"
# body
babiz=${ea_github}"/babiz/"${ex}
content_server=${ea_github}"/content-server/"${ex}
crude_monte_carlo=${ea_github}"/crude-monte-carlo/"${ex}
employment_registrar=${ea_github}"/employment-registrar/"${ex}
form_builder=${ea_github}"/form-builder/"${ex}
herolo_challenge=${ea_github}"/herolo-challenge/"${ex}
links_view=${ea_github}"/links-view/"${ex}
minister_of_transportation=${ea_github}"/minister-of-transportation/"${ex}
personal_website=${ea_github}"/personal-website/"${ex}
resume_maker=${ea_github}"/resume-maker/"${ex}
timecamp_script=${ea_github}"/timecamp-script/"${ex}
to_do_list=${ea_github}"/to-do-list/"${ex}
weather_api=${ea_github}"/weather-api/"${ex}
whatsapp_crawler=${ea_github}"/whatsapp-crawler/"${ex}
xss_example=${ea_github}"/xss-example/"${ex}
# paunch-line
ea_github_bk="${babiz} ${content_server} ${crude_monte_carlo} ${employment_registrar} ${form_builder} ${herolo_challenge} ${links_view} ${minister_of_transportation} ${personal_website} ${resume_maker} ${timecamp_script} ${to_do_list} ${weather_api} ${whatsapp_crawler} ${xss_example}"

		# 2.c ea->gitlab
# header
ea_gitlab=${ea}"/gitlab"
# body
aws_workshop=${ea_gitlab}"/aws-workshop/"${ex}
bash_scripts=${ea_gitlab}"/bash-scripts/"${ex}
challenges=${ea_gitlab}"/challenges/"${ex}
css_workshop=${ea_gitlab}"/css-workshop/"${ex}
full_stack_course=${ea_gitlab}"/full-stack-course/"${ex}
java_course=${ea_gitlab}"/java-course/"${ex}
media_uploader=${ea_gitlab}"/media-uploader/"${ex}
mern_stack_course=${ea_gitlab}"/mern-stack-course/"${ex}
spring=${ea_gitlab}"/spring/"${ex}
typescript_compiler=${ea_gitlab}"/typescript-compiler/"${ex}
# inner
#workshops=${ea_gitlab}"/workshops"
#courses=${ea_gitlab}"/courses/"
#aws_workshop=${workshops}"/aws-workshop/"${ex}
#css_workshop=${workshops}"/css-workshop/"${ex}
#full_stack_course=${courses}"/full-stack-course/"${ex}
#java_course=${courses}"/java-course/"${ex}
#mern_stack_course=${courses}"/mern-stack-course/"${ex}
# paunch-line
ea_gitlab_bk="${aws_workshop} ${bash_scripts} ${challenges} ${css_workshop} ${full_stack_course} ${java_course} ${media_uploader} ${mern_stack_course} ${spring} ${typescript_compiler}"

# 3. swap
swap=${projects}"/swap"

		# 3.a swap->gitlab
# header
swap_gitlab=${swap}"/gitlab"
# body
swap_cv=${swap_gitlab}"/swap-cv/"${ex}
# paunch-line
swap_gitlab_bk="${swap_cv}"


		# * excldues
# header
ec="--exclude="
# body
git=${ec}"*.git*"
idea=${ec}"*.idea*"
target=${ec}"*target*"
node_modules=${ec}"*node_modules*"
wp_admin=${ec}"*wp-admin*"
wp_content=${ec}"*wp-content*"
wp_includes=${ec}"*wp-includes*"
#demo_spring_boot_agpulse=${ec}"*demo-spring-boot-agpulse*"
#java_course_agpulse=${ec}"*javacourseagpulse*"
#real_spring_agpulse=${ec}"*realspringagpulse*"
#greeting_rest_spring_app_example=${ec}"*greeting-rest-spring-app-example*"
# paunch-line
#excludes="${git} ${idea} ${target} ${node_modules} ${wp_admin} ${wp_content} ${wp_includes} ${demo_spring_boot_agpulse} ${java_course_agpulse} ${real_spring_agpulse} ${greeting_rest_spring_app_example}"
excludes="${git} ${idea} ${target} ${node_modules} ${wp_admin} ${wp_content} ${wp_includes}"
	   # *				# 1.a			# 1.b		    # 1.c	      # 2.a	      # 2.b		# 2.c		  # 3.a
zip -r ${excludes} ${zip_file} ${cambium_bitbucket_bk} ${cambium_github_bk} ${cambium_gitlab_bk} ${ea_github_bk} ${ea_gitlab_bk} ${ea_bitbucket_bk} ${swap_gitlab_bk}
