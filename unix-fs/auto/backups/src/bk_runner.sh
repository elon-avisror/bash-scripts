#!/bin/bash

for script in *;
do
	if [[ -f $script && "./"$script != $0 ]];
	then
		echo $script
		filename=${script%%.*}
		source $script | grep "not matched" > "../log/"$filename".txt"
		err="$(cat ../log/"$filename".txt)"
		if [[ $err != "" ]];
		then
			echo -e "\e[31mThere are some errors:\e[0m"
			cat $err
		else
			echo -e "\e[32mSuccessfully done!\e[0m"
		fi
	fi
done

#./make_extras.sh | grep "not matched" > errors.txt
#err="$(cat ./errors.txt)"

#if [[ $err != "" ]];
#then
#	echo "there are some errors:"
#	cat ./errors.txt
#else
#	echo "backup done susccesfully"
#fi
