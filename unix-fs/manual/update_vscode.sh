#!/bin/bash

SUCCESS="\e[32m"
HINT="\e[40m"
DEFAULT="\e[0m"

echo -e "${HINT}Retrieving the real URL...${DEFAULT}"
real_url=$(curl https://update.code.visualstudio.com/latest/linux-deb-x64/stable | grep -Eo '(http|https)://[a-zA-Z0-9./?=_%:-]*')

echo -e "${HINT}Downloading the latest version of VSCode...${DEFAULT}"
curl ${real_url} --output code_latest_amd64.deb
sudo dpkg -i code_latest_amd64.deb
rm code_latest_amd64.deb

echo -e "${SUCCESS}VSCode updated successfully!${DEFAULT}"
echo -e "${HINT}Run 'code' command in the terminal to start working with the updated VSCode.${DEAFULT}"
