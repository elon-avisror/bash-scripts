#!/bin/bash

# you can run it from anywhere
cd "/home/aelon/Documents/projects"
for object in *; do

    # if this is a directory (not a file)
    if [[ -d ${object} ]]; then

        # change the directory name
        mv ${object}"/extra_info" ${object}"/extra-info"
    fi

done
