#!/bin/bash

SUCCESS="\e[32m"
HINT="\e[40m"
DEFAULT="\e[0m"

curl https://dl.pstmn.io/download/latest/linux64 --output Postman.tar.gz
sudo tar -xvzf Postman.tar.gz -C /opt
rm Postman.tar.gz

echo -e "${SUCCESS}Postman updated successfully!${DEFAULT}"
echo -e "${HINT}Restart Postman process, then run postman 'command' in the terminal to start working with the updated Postman.${DEFAULT}"
