#!/bin/bash

FILE="u=rw,g=rw,o=r"
EXEC="u=rwx,g=rwx,o=rx"
PEM="u=r,g=,o="

# $1 - is the permissions mode
# $2 - is a directory/file to change his permissions
change_mode() {
	sudo chmod $1 "$2"
}

# $1 - is a directory of files
chmod_recursive() {
	dir=("${@:1}")
	for obj in "${dir[@]}";
	do
		if [[ -d "$obj" ]];
		then
			change_mode $EXEC "$obj"
			chmod_recursive "$obj"/*
		else
			if [[ -f "$obj" ]];
			then
				echo "$obj"
				suffix="${obj##*.}"
				case $suffix in
					sh | bash | bat | cmd)
						change_mode $EXEC "$obj"
						;;
					pem)
						change_mode $PEM "$obj"
						;;
					# All other cases (txt, png, jpeg, pdf, doc, html, css, js, ts etc.)
					*)
						change_mode $FILE "$obj"
						;;
				esac
			fi
		fi
	done
}

read -p "Do you want to do so also for hidden directories and files? [Y/n] " hiddenAnswer
hiddenAnswer=${hiddenAnswer:-Y}

if [[ $hiddenAnswer = "Y" || $hiddenAnswer = "y" ]];
then
	# Show by default hidden files (turn on)
	shopt -s dotglob
fi

echo -e "\e[40mWorking on it...\e[0m"
chmod_recursive ./*
echo -e "\e[30mDone!\e[0m"

# By Default, do not show hidden directories and files (turn off)
shopt -u dotglob
