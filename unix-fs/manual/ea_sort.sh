#!/bin/bash

existed_directories=() # empty list of directories
object_suffix=""
flag=0
counter=0

for object in *; do

	# if this is a directory (not a file)
	if [[ -d $object ]]; then

		# add directory to the list of existed directories
		existed_directories+=($object)
	fi
done

for object in *; do

	# if this is a file (not a directory)
	if [[ -f $object && "./"$object != $0 ]]; then

		# cut the suffix of the file, means the location: sh, pdf, doc, txt etc.
		object_suffix=${object##*.}

		# check if the type of file has a directory or we need to create one
		for directory in ${existed_directories[@]}; do
			if [[ $directory = $object_suffix ]]; then

			# mark the file
			flag=1
			fi
		done

		# if the file has not marked
		if [[ $flag -eq 0 ]]; then

			# create directory
			mkdir $object_suffix

			# add directory to the list of existed directories
			existed_directories+=($object_suffix)
		fi

		# move the object.type to type foldier
		mv "$object" $object_suffix

		# count this movement of file
		counter=`echo "$counter + 1" | bc`

		# initialization for the next iterations
		flag=0
        fi
done

# report of the number of files that where transport
echo "The number of files that have been oredered is: "$counter
