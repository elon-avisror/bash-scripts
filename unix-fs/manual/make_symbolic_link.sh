#!/bin/bash

echo "Make sure you are on \"/opt\" folder!"
echo "Enter the name of the bash script that runs the app (and its relative-path from \"/opt\" folder)"
read sl

echo "Enter the name of the symbolic-link folder (the app that you want to be linked to \"/usr/bin\" folder)"
read app

sudo ln -s "/opt/${sl}" "/usr/bin/${app}"

