#!/bin/bash

# Mandatory
sudo tar -xvzf "$1" -C /opt

# Optional - no for Postman example
cd /opt/"$1"
./configure
make
sudo make install

# Make a symbolic-link (with ln -s command)
