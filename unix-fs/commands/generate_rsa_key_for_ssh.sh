#!/bin/bash

# $1 - is the git user email
ssh-keygen -t rsa -b 4096 -C $1
