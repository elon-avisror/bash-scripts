#!/bin/bash

sudo chmod u=rw,g=r,o=r -R $1 ./.[!.]*
sudo chmod u=rwx,g=rx,o=rx $1
