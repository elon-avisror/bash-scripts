#!/bin/bash

# Inserting this raw at the EOF "/etc/apt/sources.list"
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"

# Inserting in line 54 a comment
sudo sed -i '54 s/^/# /' /etc/apt/sources.list
