#!/bin/bash

# Link to source: "https://www.techrepublic.com/article/how-to-enable-copy-and-paste-in-virtualbox/"
sudo mount /dev/cdrom /mnt
cd /mnt
sudo apt-get install -y dkms build-essential linux-headers-generic linux-headers-$(uname -r)
sudo su
./VBoxLinuxAdditions.run
