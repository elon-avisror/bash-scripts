#!/bin/bash

google-chrome --disable-web-security --user-data-dir=/home/aelon/Documents/temps/google-chrome-not-secure >> /dev/null 2>&1 &
# If you need access to local files for dev purposes like AJAX or JSON, you can use -–allow-file-access-from-files flag.
