#!/bin/bash

nodejs() {
    # 3. nodejs (10.x) & node (latest) & nvm (0.34.0)
    curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
    sudo apt install nodejs -y
    curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
    nvm install node -y
}

nodemon() {
    # 4. nodemon (latest)
    sudo npm install -g nodemon -y
}

typescript() {
	npm install -g typescript
}

docker() {
    # 5. docker (18.09.6)
    sudo apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    sudo apt install docker-ce -y
    sudo apt install docker-ce=5:18.09.6~3-0~ubuntu-bionic -y
    sudo apt-mark hold docker-ce
    sudo usermod -aG docker $USER
}

docker_compose() {
    # 6. docker-compose (1.23.1)
    sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
}

python3() {
    # 7. pip3 (latest) & python3 (latest)
    sudo apt install python3-pip -y
}

postman() {
    # 8. postman (latest) - installed by snap
    sudo snap install postman -y
}

gcloud() {
    # 11. google-cloud-sdk (latest)

    # Add the Cloud SDK distribution URI as a package source
    echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list

    # Import the Google Cloud Platform public key
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -

    # Update the package list and install the Cloud SDK
    sudo apt-get update && sudo apt-get install google-cloud-sdk

    RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg  add - && apt-get update -y && apt-get install google-cloud-sdk -y
    # Add the Cloud SDK distribution URI as a package source
    echo "deb http://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list

    # Import the Google Cloud Platform public key
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

    # Update the package list and install the Cloud SDK
    sudo apt-get update && sudo apt-get install google-cloud-sdk -y

    # NOTE: the user needs to initliaze gcloud for him
}

aws_cli() {
    # 12. aws-cli (latest)
    sudo pip3 install awscli

    # NOTE: the user needs to initialize aws for him (use: "aws configure" command)
}

maven() {
    # 13. maven
    sudo apt-get install maven -y

    # Installing JDK and JRE
    sudo apt-get install -y default-jre
    sudo apt-get install -y default-jdk
}

postgres() {
	# Create the file repository configuration:
	sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

	# Import the repository signing key:
	wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

	# Update the package lists:
	sudo apt update

	# Install the latest version of PostgreSQL.
	# If you want a specific version, use 'postgresql-12' or similar instead of 'postgresql':
	sudo apt -y install postgresql
}

pgadmin() {
	#
	# Setup the repository
	#

	# Install the public key for the repository (if not done previously):
	curl https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo apt-key add

	# Create the repository configuration file:
	sudo sh -c 'echo "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list && apt update'

	#
	# Install pgAdmin
	#

	# Install for both desktop and web modes:
	sudo apt install pgadmin4

	# Install for desktop mode only:
	sudo apt install pgadmin4-desktop

	# Install for web mode only:
	sudo apt install pgadmin4-web

	# Configure the webserver, if you installed pgadmin4-web:
	sudo /usr/pgadmin4/bin/setup-web.sh
}

# $1 - function name
import_function() {
	# Check if the function exists (bash specific)
	if [[ declare -f "$1" > /dev/null ]];
	then
		# Call arguments verbatim
		"$@"
	else
		# Show a helpful error
		echo "'$1' is not a known function name" >&2
		exit 1
	fi
}

import_function $1
