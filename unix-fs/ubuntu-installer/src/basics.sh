#!/bin/bash

base() {
    # 0. apt update & upgrade & install base-tools
    sudo apt update && sudo apt upgrade && sudo apt install curl wget xclip tree -y
}

google() {
    # 1. google-chrome-stable (latest)
    wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
    sudo apt install ./google-chrome-stable_current_amd64.deb
    rm google-chrome-stable_current_amd64.deb
}

git() {
    # 2. git (latest)
    sudo apt install git -y
}

vscode() {
    # 9. vscode (latest)
    sudo apt install software-properties-common apt-transport-https wget -y
    wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
    sudo apt install code -y
}

jetbrains_toolbox() {
    # 10.jetbrains-toolbox (1.16.6319)
    wget -c https://download.jetbrains.com/toolbox/jetbrains-toolbox-1.16.6319.tar.gz jetbrains-toolbox.tar.gz
    sudo tar -xzf jetbrains-toolbox.tar.gz -C /opt
    sudo /opt/jetbrains-toolbox/jetbrains-toolbox
}

# $1 - function name
import_function() {
	# Check if the function exists (bash specific)
	if [[ declare -f "$1" > /dev/null ]];
	then
		# Call arguments verbatim
		"$@"
	else
		# Show a helpful error
		echo "'$1' is not a known function name" >&2
		exit 1
	fi
}

import_function $1
