#!/bin/bash

# 0. Update & Upgrade APT, Installing some Basic-tools ("curl", "wget", "xclip", "tree", "net-tools", "gnome-tweaks", "gitk")
sudo apt update && sudo apt upgrade -y && sudo apt install curl wget xclip tree net-tools gnome-tweaks gitk -y

# 1. Installing "google-chrome-stable" (latest)
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i ./google-chrome-stable_current_amd64.deb
rm google-chrome-stable_current_amd64.deb

# 2. Installing "git" (latest)
sudo apt install git -y

# 3. Installing "nodejs" (10.x version) & "node" (latest version) & "nvm" (0.34.0 version)
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt install nodejs -y
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
. ~/.bashrc
nvm install node -y

# 4. Installing "nodemon" (latest version)
sudo npm install -g nodemon -y

# 5. Installing "docker" (18.09.6 version)
sudo apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt install docker-ce -y
sudo apt install docker-ce=5:18.09.6~3-0~ubuntu-bionic -y
sudo apt-mark hold docker-ce
sudo usermod -aG docker $USER

# 6. docker-compose (1.23.1)
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# 7. pip3 (latest) & python3 (latest)
sudo apt install python3-pip -y

# 8. postman (latest) - installed by snap
sudo snap install postman

# 9. vscode (latest)
sudo apt install software-properties-common apt-transport-https -y
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
sudo apt install code -y

# 10.jetbrains-toolbox (1.16.6319)
wget -c https://download.jetbrains.com/toolbox/jetbrains-toolbox-1.16.6319.tar.gz --output jetbrains-toolbox.tar.gz
sudo tar -xzf jetbrains-toolbox.tar.gz -C /opt
rm jetbrains-toolbox.tar.gz
sudo mv /opt/jetbrains-toolbox-1.16.6319/ /opt/jetbrains-toolbox
sudo /opt/jetbrains-toolbox/jetbrains-toolbox

# 11. google-cloud-sdk (latest)
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
sudo apt install google-cloud-sdk -y
# NOTE: the user needs to initliaze gcloud for him

# 12. aws-cli (latest)
sudo pip3 install awscli
# NOTE: the user needs to initialize aws for him (use: "aws configure" command)

# 13. maven with jdk and jre (latest)
sudo apt install maven default-jdk default-jre -y
