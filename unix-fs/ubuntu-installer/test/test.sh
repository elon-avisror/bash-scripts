#!/bin/bash

basics=("test1" "test2")
frontend=("jj")
backend=("btest" "bbtest")
fullstack=(${frontend[@]}" "${backend[@]})
echo ${basics[@]}
echo ${fullstack[@]}

# $1 - container name
# $2 - package installion name
function install_package {
	read -p "Do you want to install $2 package? [Y/n] " answer
	packageAnswer=${answer:-Y}
	if [[ $packageAnswer == "Y" || $packageAnswer == "y" ]];
	then
		# install from "src/$1.sh" script file "$2" package
		source "src/$1.sh" $2
	fi
}

# $1 - container name
# $2 - container of packages
function run_container {
	read -p "Do you want to install the $1 container? [y/N] " answer
	containerAnswer=${answer:-N}
	if [[ $containerAnswer == "y" || $containerAnswer == "Y" ]];
	then
		container=("${@:2}")
		echo ${container[@]}
		for package in ${container[@]};
		do
			install_package $1 $package
		done
	fi
}

run_container "basics" "${basics[@]}"

read -p "Are you a fullstack developer? [y/N] " fullstackAnswer
answer=${fullstackAnswer:-N}
if [[ $answer == "y" || $answer == "Y" ]];
then
	run_container "fullstack" $fullstack
else
	run_container "frontend" $frontend
	run_container "backend" $backend
fi
