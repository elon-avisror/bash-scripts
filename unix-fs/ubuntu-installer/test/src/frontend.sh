#!/bin/bash

jj() {
	echo "I'm JJ"
}

# $1 - function name
import_function() {
	# Check if the function exists (bash specific)
	if declare -f "$1" > /dev/null
	then
		# Call arguments verbatim
		"$@"
	else
		# Show a helpful error
		echo "'$1' is not a known function name" >&2
		exit 1
	fi
}

import_function $1