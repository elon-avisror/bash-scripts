#!/bin/bash

test1() {
	echo "I'm test 1"
}

test2() {
	echo "T"
}

check() {
	# Check if the function exists (bash specific)
	if declare -f "$1" > /dev/null
	then
		# call arguments verbatim
		"$@"
	else
		# Show a helpful error
		echo "'$1' is not a known function name" >&2
		exit 1
	fi
}

check $1
