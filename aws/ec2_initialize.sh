#!/bin/bash

yum update -y
yum install httpd -y
service httpd start
chkconfig httpd on
cd /var/www/html
echo "<html><h1>Hello from AWS EC2</h1></html>" > index.html
