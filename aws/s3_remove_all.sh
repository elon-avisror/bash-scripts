#!/bin/bash

buckets=("gmunch-profiles" "gmunch-products" "gmunch-stores")

function remove_all {
	aws s3 rm s3://$1 --recursive
}

# Example of removing with 'doc' prefix: aws s3 rm s3://bucket-name/doc --recursive
for bucket in ${buckets[@]}
do
	remove_all "$bucket"
done
