#!/bin/bash

function print {
	echo -e "\e[100m$1\e[0m"
}

print "fetch from s3"
wget https://elon-avisror.s3-eu-west-1.amazonaws.com/personal-website-frontend-master.zip

print "unzip it"
unzip personal-website-frontend-master.zip

print "clean & update"
sudo rm -r /var/www/html/*
sudo mv personal-website-frontend-master/* /var/www/html/

print "clear"
rm personal-website-frontend-master.zip
rmdir personal-website-frontend-master
