#!/bin/bash

# in order to kill the proccess with the id that exploding data > syslog
# we first execute, 'tail -f syslog' command will show you the tailed data that explode it
# then, get the proceess id
# finally, using 'ps aux' command to find that proccess id
# and kill it using 'kill [PROCESS_ID]'

read syslog_file

process_number='\[[0-9][0-9][0-9][0-9]\]'

if [[ ${syslog_file} =~ ${process_number} ]]
then
	# TODO: add kill the process numbers
	processes_to_kill=$(echo "${syslog_file} =~ ${proccess_number}" | grep -oP "\d\d\d\d")
	for process in "${processes_to_kill[@]}"
	do
		sudo kill ${process}
	done
fi

# now you can clear syslog file

sudo truncate -s 0 /var/log/syslog
