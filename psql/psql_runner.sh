#!/bin/bash
function run_script {
	sudo -u postgres psql gmunch < ${path_to_script}$1
}

echo -e "\e[40mOptional functions: create/delete\e[0m"
read -p "What function from the above do you want to do? [C/d] " input
input=${input:-C}

path_to_script="/home/aelon/Documents/projects/cambium/bitbucket/gmunch/gmunch-core/src/dal/psql/scripts/"

if [[ $input == "C" || $input == "c" ]];
then
	run_script "create.sql"
	run_script "meta.sql"
else
	run_script "delete.sql"
fi
