#!/bin/bash

project="../gmunch-core/"
extras=${pwd}"bak-extras/"

# $1 - is the filename with his relative path (if exists)
function copy {
	cp ${project}$1 ${extras}$2
}

copy ".env"
copy "src/dev_index.ts"
copy "src/dal/psql/scripts/insert.sql"
