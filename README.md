# Bash Scripts

## Generic Scripts

Built for helpful automation, you can freely enjoy...

## Ubuntu Installer Script

### Introduction

This is an ubuntu shell script to setup a basic development environment.
I have created this script to setup development environment after fresh installation of Ubuntu.
This scriopt will install the following softwares and tools for you (links with guides from Linuxize | Original Source):

1. [google-chrome-stable (latest)](https://linuxize.com/post/how-to-install-google-chrome-web-browser-on-ubuntu-18-04/)
2. [git (latest)](https://linuxize.com/post/how-to-install-git-on-ubuntu-18-04/)
3. [nodejs (10.x) & npm (latest) & nvm (0.34.0)](https://linuxize.com/post/how-to-install-node-js-on-ubuntu-18.04/)
4. [nodemon (latest)](https://www.npmjs.com/package/nodemon)
5. [docker (18.09.6)](https://linuxize.com/post/how-to-install-and-use-docker-on-ubuntu-18-04/)
6. [docker-compose (1.23.1)](https://linuxize.com/post/how-to-install-and-use-docker-compose-on-ubuntu-18-04/)
7. [pip3 (latest) & python3 (latest)](https://linuxize.com/post/how-to-install-python-3-8-on-ubuntu-18-04/)
8. [postman (latest)](https://linuxize.com/post/how-to-install-postman-on-ubuntu-18-04/) - installed by snap
9. [vscode (latest)](https://linuxize.com/post/how-to-install-visual-studio-code-on-ubuntu-18-04/)
10. [jetbrains-toolbox (1.16.6319)](https://www.jetbrains.com/toolbox-app/)
11. [google-cloud-sdk (latest)](https://cloud.google.com/sdk/docs/quickstart-debian-ubuntu)

### Download

    wget -c https://elon-avisror.s3-eu-west-1.amazonaws.com/ubuntu_installer.sh -O ubuntu_installer.sh

### Execution

    sudo chmod +x ubuntu_installer.sh
    ./ubuntu_installer.sh
